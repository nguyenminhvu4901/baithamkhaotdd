<?php

namespace Database\Factories;

use App\Models\Task;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    use WithFaker;

    protected $model = Task::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'status' => $this->faker->text,
        ];
    }
}
