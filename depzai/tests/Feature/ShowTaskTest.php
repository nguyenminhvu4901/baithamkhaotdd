<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowTaskTest extends TestCase
{
    public function getDetailTaskRoute($id)
    {
        return route('tasks.show', ['id' => $id]);
    }

    /** @test */
    public function user_can_get_task_if_task_is_exist()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getDetailTaskRoute($task->id));

        $response->assertStatus(200);
        $response->assertViewHas('task', $task);
        $response->assertSee($task->name);
        $response->assertSee($task->status);
    }

     /** @test */
     public function user_can_not_get_task_if_task_is_not_exist()
     {
         $task = -1;
         $response = $this->get($this->getDetailTaskRoute($task));
 
         $response->assertStatus(404);
     }
}
