<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    public function getUpdateTaskRoute($id)
    {
        return route('tasks.update', ['id' => $id]);
    }

    public function getUpdateTaskViewRoute($id)
    {
        return route('tasks.edit', ['id' => $id]);
    }

    public function getListTaskRoute()
    {
        return route('tasks.index');
    }

    /** @test */
    public function authenticate_user_can_update_task_if_data_is_valid()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->toArray();
        $response = $this->from($this->getUpdateTaskViewRoute($task['id']))->put($this->getUpdateTaskRoute($task['id']), $task);
        $task = Task::findOrFail($task['id'])->toArray();

        $response->assertStatus(302);
        $response->assertRedirect($this->getListTaskRoute());
        $this->assertDatabaseHas('tasks', $task);
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_task_is_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $task = -1;
        $response = $this->get($this->getUpdateTaskViewRoute($task));

        $response->assertStatus(404);
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_data_is_not_valid()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->toArray();
        $task['name'] = 1;
        $task['status'] = '';
        $response = $this->from($this->getUpdateTaskViewRoute($task['id']))->put($this->getUpdateTaskRoute($task['id']), $task);

        $response->assertStatus(302);
        $response->assertRedirect($this->getUpdateTaskViewRoute($task['id']));
        $response->assertSessionHasErrors(['name', 'status']);
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_name_is_not_valid()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->toArray();
        $task['name'] = 21421;
        $response = $this->from($this->getUpdateTaskViewRoute($task['id']))->put($this->getUpdateTaskRoute($task['id']), $task);

        $response->assertStatus(302);
        $response->assertRedirect($this->getUpdateTaskViewRoute($task['id']));
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_status_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->toArray();
        $task['status'] = '';
        $response = $this->from($this->getUpdateTaskViewRoute($task['id']))->put($this->getUpdateTaskRoute($task['id']), $task);

        $response->assertStatus(302);
        $response->assertRedirect($this->getUpdateTaskViewRoute($task['id']));
        $response->assertSessionHasErrors(['status']);
    }

    /** @test */
    public function unauthenticate_user_can_not_update_task()
    {
        $task = Task::factory()->create()->toArray();
        $response = $this->from($this->getUpdateTaskViewRoute($task['id']))->put($this->getUpdateTaskRoute($task['id']), $task);

        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function unauthenticate_user_can_not_see_update_task_form()
    {
        $task = Task::factory()->create()->toArray();
        $response = $this->get($this->getUpdateTaskViewRoute($task['id']));

        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_see_update_task_form()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get($this->getUpdateTaskViewRoute($task));

        $response->assertStatus(200);
        $response->assertViewHas('task', $task);
        $response->assertSee($task->name);
        $response->assertSee($task->status);
    }
}
