<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateRequest;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $task;
    
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function index()
    {
        $tasks = $this->task->all();
        return view('tasks.index', compact('tasks'));
    }

    public function show($id)
    {
        $task = $this->task->findOrFail($id);
        return view('tasks.show', compact('task'));
    }

    public function edit($id)
    {
        $task = $this->task->findOrFail($id);
        return view('tasks.edit', compact('task'));
    }

    public function update(UpdateRequest $request, $id)
    {
        $data = $request->all();
        $task = $this->task->findOrFail($id);
        $task->update($data);
        return redirect()->route('tasks.index');
    }


}
